def count_men(text, sub1=" men", sub2=" Men"):
    """counts the number of times " men" or " Men" is in the text

    :param text: input text from read_text
    :param sub1: first substring to search
    :param sub2: second substring to search
    :return: number of instances
    """
    pos = 0
    count = 0
    while pos < len(text):
        instance1 = text[pos:].find(sub1)
        instance2 = text[pos:].find(sub2)
        if instance1 == 0:
            count += 1
        if instance2 == 0:
            count += 1
        pos += 1
    return count


def read_text(filename):
    with open(filename, 'r') as f:
        line1 = f.readline()
        line2 = f.readlines()
    no_newline = ""
    for i in range(len(line2)):
        no_newline += line2[i].rstrip()
        no_newline += "\n"
    return no_newline


def capitalize_women(text):
    cap_women = str.replace(text, 'women', 'WOMEN')
    print(cap_women)
    new_file = open("example_text_new.txt", 'w')
    new_file.write(cap_women)


def contains_blue_devil(text):
    ind = str.find(text, 'Blue Devil')
    if ind == -1:
        print('There are no instances of "Blue Devil" in this text')
    else:
        print('The words "Blue Devil" are found in this text')


if __name__ == "__main__":
    text = read_text('example_text.txt')
    capitalize_women(text)
    contains_blue_devil(text)
    # print([method for method in dir(x) if callable(getattr(x,method))])
